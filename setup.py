from setuptools import find_packages, setup

with open('requirements.txt') as fp:
    install_requires = fp.read()

setup(
    name="data_pipeline",
    version="0.1.0",
    author="John Troumpis",
    description="A tool to read data from an http source and post them to a PG database.",
    url='https://gitlab.com/jtroumpis/drug-target-commons-pipeline',
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    packages=find_packages(),
    entry_points={'console_scripts': ['datapipe=data_pipeline.cli']},
    install_requires=install_requires,
)
