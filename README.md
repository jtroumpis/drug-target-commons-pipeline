# DTC Data Pipeline

This is a tool to read data from [Drug Target Commons](https://drugtargetcommons.fimm.fi) and store them to a PostgreSQL database.


## Installation

Using pip, the Python package manager:
```
pip install .
```
or using `setup.py`:
```
python setup.py install
```

A python3 environment is required, as well as an internet connection.
The PostgreSQL needs to be >= v9.5
## Usage

**Important** 
In order to use the DB_URI of the database to connect to needs to be given either via 
the `DATABASE_URI` env variable or by passing it during execution with the `--uri` flag.

```
# Collect all available data
datapipe --uri postgres://USERNAME:PASSWORD@babar.elephantsql.com:5432/my_drug_database
```

```
# Limit the number of recoreds per request
datapipe --limit 20
```
_**Note**: The DTC API sets a maximum limit of 500_

```
# Start and end the requests from a specific record
datapipe --start 42 --stop 420
```

## Notes
SQL Alchemy was used, so the program is *mostly* database agnostic. However, some optiomisations regarding bulk upsert 
that are Postgres specific have been implemented. It might work with other languages that implement similar dialect.


