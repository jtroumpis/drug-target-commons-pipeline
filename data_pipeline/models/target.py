from sqlalchemy import Column, Integer, ForeignKey, UniqueConstraint
from data_pipeline.models.base import DefaultFields, Base

from sqlalchemy.orm import relationship


class TargetPrefName(Base, DefaultFields):
    __tablename__ = 'pref_name'


class Organism(Base, DefaultFields):
    __tablename__ = 'organism'


class Target(Base):
    __tablename__ = 'target'

    id = Column(Integer, primary_key=True)
    pref_name_id = Column(Integer, ForeignKey('pref_name.id'))
    pref_name = relationship("TargetPrefName")
    organism_id = Column(Integer, ForeignKey('organism.id'))
    organism = relationship("Organism")
    uix = UniqueConstraint('pref_name', 'organism')
