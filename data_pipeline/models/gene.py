from data_pipeline.models.base import Base, DefaultFields


class Gene(Base, DefaultFields):
    __tablename__ = 'gene'
