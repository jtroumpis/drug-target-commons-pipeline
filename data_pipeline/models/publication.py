from sqlalchemy import Column, Integer, Table, ForeignKey
from data_pipeline.models.base import Base, DefaultFields
from sqlalchemy.orm import relationship

association_table = Table('association', Base.metadata,
                          Column('author_id', Integer, ForeignKey('author.id')),
                          Column('publication_id', Integer, ForeignKey('publication.id'))
                          )


class Author(Base, DefaultFields):
    __tablename__ = 'author'


class Publication(Base):
    __tablename__ = 'publication'

    id = Column(Integer, primary_key=True)
    pubmed_id = Column(Integer, nullable=False)
    authors = relationship("Author", secondary=association_table)
