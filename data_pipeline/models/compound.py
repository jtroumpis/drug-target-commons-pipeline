from data_pipeline.models.base import DefaultFields, Base


class Compound(Base, DefaultFields):
    __tablename__ = 'compound'
