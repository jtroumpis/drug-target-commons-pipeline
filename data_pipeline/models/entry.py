from sqlalchemy import Column, Integer, ForeignKey, JSON
from data_pipeline.models.base import Base
from sqlalchemy.orm import relationship


class Entry(Base):
    __tablename__ = 'entry'

    id = Column(Integer, primary_key=True)
    target_id = Column(Integer, ForeignKey('target.id'))
    target = relationship("Target")
    gene_id = Column(Integer, ForeignKey('gene.id'))
    gene = relationship("Gene")
    compound_id = Column(Integer, ForeignKey('compound.id'))
    compound = relationship("Compound")
    publication_id = Column(Integer, ForeignKey('publication.id'))
    publication = relationship("Publication")
    data = Column(JSON)  # Why not?
