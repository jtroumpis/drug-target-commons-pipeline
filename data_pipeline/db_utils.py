from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from data_pipeline.models.base import Base
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy_utils import database_exists, create_database


def insert_unique(session, model, data):
    ins_query = insert(model).values(data)
    ups_query = ins_query.on_conflict_do_nothing()
    session.execute(ups_query)
    session.commit()


def create_engine_and_session(uri):
    engine = create_engine(uri)
    if not database_exists(engine.url):
        create_database(engine.url)

    Base.metadata.create_all(engine)

    return sessionmaker(bind=engine)()
