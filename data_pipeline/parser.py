from data_pipeline.models.compound import Compound
from data_pipeline.models.entry import Entry
from data_pipeline.models.gene import Gene
from data_pipeline.models.publication import Author, Publication
from data_pipeline.models.target import Organism, TargetPrefName, Target
from data_pipeline.db_utils import insert_unique, create_engine_and_session
import os
import logging

logger = logging.getLogger()
session = None


def set_up(uri):
    global session
    session = create_engine_and_session(uri or os.environ.get("DATABASE_URI"))
    print("Connected to DB")


def handle_unique_names(key, model, data):
    name_list = [{"name": i.get(key)} for i in data if i.get(key)]
    insert_unique(session, model, name_list)


def create_entry(data):
    organism = session.query(Organism).filter_by(name=data["target_organism"]).first()
    pref_name = session.query(TargetPrefName).filter_by(name=data["target_pref_name"]).first()

    target = Target(organism=organism, pref_name=pref_name)
    session.merge(target)

    names = (data.get("authors", "") or "").split(",")

    authors = [session.query(Author).filter_by(name=auth_name).first() for auth_name in names if auth_name]

    if data.get("pubmed_id"):
        pub = Publication(
            pubmed_id=data.get("pubmed_id"),
            authors=authors
        )

        session.merge(pub)
    else:
        pub = None

    gene = session.query(Gene).filter_by(name=data["gene_name"]).first()

    compound = session.query(Compound).filter_by(name=data["compound_name"]).first()

    e = Entry(
        id=data["resource_uri"].split("/")[-2],
        target=target,
        gene=gene,
        compound=compound,
        data=data,
        publication=pub
    )

    session.merge(e)


def parse_and_save(data):
    try:
        handle_unique_names("target_organism", Organism, data)
        handle_unique_names("target_pref_name", TargetPrefName, data)
        handle_unique_names("gene_name", Gene, data)
        handle_unique_names("compound_name", Compound, data)

        author_set = set()
        for i in data:
            authors = (i.get("authors") or "").split(",")
            author_set = author_set.union(set(authors))
        authors = [{"name": author} for author in author_set if author]

        insert_unique(session, Author, authors)

        for item in data:
            create_entry(item)

        session.commit()
    finally:
        session.close()
