from requests import get
from requests.exceptions import HTTPError
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

BASE_URL = "https://drugtargetcommons.fimm.fi/api/data/bioactivity/"


def make_request(url, limit, offset):
    params = {"format": "json", "limit": limit, "offset": offset}

    try:
        res = get(url, verify=False, params=params)
    except Exception as e:
        print(str(e))
        raise
    logger.info(f"{res.status_code}: {res.text}")
    return res.json()


def get_all(limit, offset=0, end=None):
    data = []
    while True:
        try:
            res = make_request(BASE_URL, limit, offset)
            data.extend(res["bioactivities"])
        except HTTPError as e:
            logger.error(f"Failed to get more data: {str(e)}")
            break

        offset += limit

        if res["meta"]["total_count"] <= offset or end and offset >= end:
            break

    logger.info(f"Finished collecting. Collected {len(data)} data points.")
    return data
