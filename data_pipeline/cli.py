import argparse
from data_pipeline.parser import parse_and_save, set_up
from data_pipeline.http_client import get_all
import logging

logging.basicConfig()

parser = argparse.ArgumentParser()
parser.add_argument('--limit', help='How many records to request per call', type=int, default=500)
parser.add_argument('--start', help='At which record to start', type=int, default=0)
parser.add_argument('--stop', help='At which record to end', type=int, default=None)
parser.add_argument('--uri', help='Set the DATABASE_URI')
args = parser.parse_args()

set_up(args.uri)

print("Started...")
data = get_all(args.limit, args.start, args.stop)

parse_and_save(data)
print("Update finished successfully")
